//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 1992-2015 Andras Varga
// Copyright (C) 2020 Ruben Danilo Capkob. (danilocapkob@gmail.com)
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//
#ifdef _MSC_VER
#pragma warning(disable : 4786)
#endif

#include <vector>
#include <omnetpp.h>
#include "Packet_m.h"

using namespace omnetpp;

std::vector<int> globhopcountVector;
int globhopcount = 0;

class Core : public cSimpleModule
{
private:
    int myAddress;
    std::vector<int> destAddresses;
    cPar *packetLengthBytes;

    cMessage *generatePacket;
    long pkCounter;
    simtime_t timeOut;

public:
    Core();
    virtual ~Core();

protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;

    virtual Packet *generateMessage(int dstAddress);
    virtual void backwardMessage(Packet *pk);
    virtual void forwardMessage(Packet *pk);
};

Define_Module(Core);

Core::Core()
{
    generatePacket = nullptr;
}

Core::~Core()
{
    cancelAndDelete(generatePacket);
}

void Core::initialize()
{
    myAddress = par("address");
    packetLengthBytes = &par("packetLength");
    pkCounter = 0;

    WATCH(pkCounter);
    WATCH(myAddress);

    const char *destAddressesPar = par("destAddresses");
    cStringTokenizer tokenizer(destAddressesPar);
    const char *token;
    int k = 0;
    while ((token = tokenizer.nextToken()) != nullptr)
    {
        destAddresses.push_back(atoi(token));
        k++;
    }

    if (destAddresses.size() == 0)
        throw cRuntimeError("At least one address must be "
                            "in the destAddresses parameter!");

    if (myAddress == 0)
    { // the only node that sends packets is the "zero" node
        timeOut = 0.002;
        generatePacket = new cMessage("nextPacket");
        scheduleAt(0, generatePacket);
    }
}

void Core::handleMessage(cMessage *msg)
{
    if (msg == generatePacket)
    { // Sending and generate packet
        int destAddress = destAddresses.size() - 1;
        // count start packet
        globhopcountVector.push_back(0);

        Packet *pk = generateMessage(destAddress);
        send(pk, "outStore");
        scheduleAt(simTime() + timeOut, generatePacket);

        if (hasGUI())
        {
            getParentModule()->bubble("Generating packet...");
        }
    }
    else
    { // Handle incoming packet
        Packet *pk = check_and_cast<Packet *>(msg);
        if (myAddress == destAddresses.size() - 1)
        { // final node
            backwardMessage(pk);
            send(pk, "outStore");
        }
        else if (myAddress == pk->getDestAddr())
        { // message is an "ACK"
            send(pk, "outStore");
        }
        else
        {
            backwardMessage(pk);
            forwardMessage(pk);
        }
    }
}

Packet *Core::generateMessage(int dstAddress)
{
    char pkname[40];
    sprintf(pkname, "pk-%d-to-%d-#%ld", myAddress, dstAddress, pkCounter);
    EV << "generating packet " << pkname << endl;

    Packet *pk = new Packet(pkname);
    pk->setByteLength(packetLengthBytes->intValue());
    pk->setKind(intuniform(0, 7));
    pk->setSrcAddr(myAddress);
    pk->setDestAddr(dstAddress);
    pk->setNumberPk(pkCounter);
    pk->setSchedulingPriority(1);
    pkCounter++;
    return pk;
}

void Core::backwardMessage(Packet *pk)
{
    int dstAddrs = pk->getSrcAddr();
    long numberPk = pk->getNumberPk();

    Packet *copyBackward = (Packet *)pk->dup();

    char pkname[40];
    sprintf(pkname, "ack-%d-to-%d-#%ld", myAddress, dstAddrs, numberPk);
    EV << "generating packet backward " << pkname << endl;

    copyBackward->setName(pkname);
    copyBackward->setSrcAddr(myAddress);
    copyBackward->setDestAddr(dstAddrs);
    copyBackward->setSchedulingPriority(0);

    send(copyBackward, "outExit");
}

void Core::forwardMessage(Packet *pk)
{
    int dstAddrs = pk->getDestAddr();
    long numberPk = pk->getNumberPk();

    char pkname[40];
    sprintf(pkname, "pk-%d-to-%d-#%ld", myAddress, dstAddrs, numberPk);
    EV << "generating packet forward " << pkname << endl;

    pk->setName(pkname);
    pk->setSrcAddr(myAddress);
    pk->setDestAddr(dstAddrs);
    pk->setSchedulingPriority(1);

    send(pk, "outStore");
}
