//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 1992-2015 Andras Varga
// Copyright (C) 2020 Ruben Danilo Capkob. (danilocapkob@gmail.com)
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//
#ifdef _MSC_VER
#pragma warning(disable : 4786)
#endif

#define FSM_DEBUG

#include <string.h>
#include <map>
#include <omnetpp.h>
#include "Packet_m.h"

using namespace omnetpp;

extern std::vector<int> globhopcountVector;
extern int globhopcount;

class Routing : public cSimpleModule
{
private:
    int myAddress;

    cPar *sleepTime;
    cPar *activeTime;

    // state
    cFSM fsm;
    enum
    {
        INIT = 0,
        DOWN = FSM_Steady(1),
        UP = FSM_Steady(2),
        SEND = FSM_Transient(1),
    };

    cMessage *chState;

    typedef std::map<int, int> RoutingTable;
    RoutingTable rtable;

    simsignal_t dropSignal;
    simsignal_t outputIfSignal;

public:
    Routing();
    virtual ~Routing();

protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;

    virtual void processTimer(cMessage *msg);
    virtual void processPacket(Packet *pk);
};

Define_Module(Routing);

Routing::Routing()
{
    chState = nullptr;
}

Routing::~Routing()
{
    cancelAndDelete(chState);
}

void Routing::initialize()
{
    myAddress = getParentModule()->par("address");
    sleepTime = &par("sleepTime");
    activeTime = &par("activeTime");
    dropSignal = registerSignal("drop");
    outputIfSignal = registerSignal("outputIf");

    //
    // Brute force approach -- every node does topology discovery on its own,
    // and finds routes to all other nodes independently, at the beginning
    // of the simulation. This could be improved: (1) central routing database,
    // (2) on-demand route calculation
    //
    cTopology *topo = new cTopology("topo");

    std::vector<std::string> nedTypes;
    std::string var = getParentModule()->getNedTypeName();
    nedTypes.push_back(var);

    topo->extractByNedTypeName(nedTypes);
    EV << "cTopology found " << topo->getNumNodes() << " nodes\n";

    cTopology::Node *thisNode = topo->getNodeFor(getParentModule());

    // find and store next hops
    for (int i = 0; i < topo->getNumNodes(); i++)
    {
        if (topo->getNode(i) == thisNode)
        {
            continue; // skip ourselves
        }

        topo->calculateUnweightedSingleShortestPathsTo(topo->getNode(i));

        if (thisNode->getNumPaths() == 0)
        {
            continue; // not connected
        }

        cGate *parentModuleGate = thisNode->getPath(0)->getLocalGate();

        int gateIndex = parentModuleGate->getIndex();
        int address = topo->getNode(i)->getModule()->par("address");
        rtable[address] = gateIndex;
    }
    delete topo;

    fsm.setName("fsm");

    if (myAddress == 2)
    {
        chState = new cMessage("chState");
        scheduleAt(0, chState);
    }
}

void Routing::handleMessage(cMessage *msg)
{
    // process the self-message or incoming packet
    if (myAddress == 2)
    {
        if (msg->isSelfMessage())
        {
            processTimer(msg);
        }
        else
        {
            Packet *pk = check_and_cast<Packet *>(msg);
            if (myAddress != pk->getSrcAddr())
            {
                EV << "\nEstado de la FSM: " << fsm.getStateName() << endl;
                if (strcmp(fsm.getStateName(), "UP") == 0)
                {
                    send(pk, "localOut");
                }
                else if (strcmp(fsm.getStateName(), "DOWN") == 0)
                {
                    bubble("Msg Lost");

                    int index = pk->getNumberPk();
                    delete msg;
                }
            }
            else
                processPacket(pk);
        }
    }
    else
    {
        Packet *pk = check_and_cast<Packet *>(msg);
        if (myAddress != pk->getSrcAddr())
        {
            send(pk, "localOut");
        }
        else
            processPacket(pk);
    }
}

void Routing::processTimer(cMessage *msg)
{
    simtime_t d;
    FSM_Switch(fsm)
    {
    case FSM_Exit(INIT):
        // transition to DOWN state
        FSM_Goto(fsm, DOWN);
        break;

    case FSM_Enter(DOWN):
        // schedule end of sleep period
        cancelEvent(chState);
        d = sleepTime->doubleValue();
        scheduleAt(simTime() + d, chState);

        // display message
        EV << "sleeping for " << d << "s\n";

        getDisplayString().setTagArg("i", 1, "");
        break;

    case FSM_Exit(DOWN):
        // schedule end of this burst
        cancelEvent(chState);
        d = activeTime->doubleValue();
        scheduleAt(simTime() + d, chState);

        // display message
        EV << "starting active of duration " << d << "s\n";

        getDisplayString().setTagArg("i", 1, "yellow");

        // transition to UP state:
        if (msg != chState)
        {
            throw cRuntimeError("invalid event in state UP");
        }
        FSM_Goto(fsm, UP);
        break;

    case FSM_Enter(UP):
        EV << "waiting out"
           << "\n";
        break;

    case FSM_Exit(UP):
        FSM_Goto(fsm, DOWN);
        break;
    }
}

void Routing::processPacket(Packet *pk)
{
    int destAddr = pk->getDestAddr();

    if (destAddr == myAddress)
    {
        EV << "local delivery of packet " << pk->getName() << endl;
        send(pk, "localOut");
        emit(outputIfSignal, -1); // -1: local
        return;
    }

    RoutingTable::iterator it = rtable.find(destAddr);
    if (it == rtable.end())
    {
        EV << "address " << destAddr
           << " unreachable, discarding packet " << pk->getName()
           << endl;
        emit(dropSignal, (long)pk->getByteLength());
        delete pk;
        return;
    }

    int outGateIndex = (*it).second;
    EV << "sending packet " << pk->getName()
       << " on gate index " << outGateIndex << endl;

    emit(outputIfSignal, outGateIndex);
    send(pk, "out", outGateIndex);
}
