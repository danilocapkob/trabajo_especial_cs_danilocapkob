//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 1992-2015 Andras Varga
// Copyright (C) 2020 Ruben Danilo Capkob. (danilocapkob@gmail.com)
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//
#include <vector>
#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
#include "Packet_m.h"

using namespace omnetpp;

extern std::vector<int> globhopcountVector;
extern int globhopcount;

class Bundle : public cSimpleModule
{
private:
    int myAddress;
    int numPkNext;
    int numPkCurrent;

    std::vector<int> destAddresses;

    cArray buffer;
    cMessage *endTransmissionEvent;

    bool isBusy;

    simtime_t timeOut;
    simsignal_t alenSignal;
    simsignal_t busySignal;
    simsignal_t endToEndDelaySignal;
    simsignal_t arrivalSignal;

public:
    Bundle();
    virtual ~Bundle();

protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
    virtual void refreshDisplay() const override;
    virtual void startTransmitting(Packet *pk);
};

Define_Module(Bundle);

Bundle::Bundle()
{
    endTransmissionEvent = nullptr;
}

Bundle::~Bundle()
{
    cancelAndDelete(endTransmissionEvent);
}

void Bundle::initialize()
{
    buffer.setName("buffer");
    endTransmissionEvent = new cMessage("endTxEvent");
    endTransmissionEvent->setSchedulingPriority(2);

    myAddress = getParentModule()->par("address");

    numPkCurrent = 0;
    numPkNext = 1;

    const char *destAddressesPar = par("destAddresses");
    cStringTokenizer tokenizer(destAddressesPar);
    const char *token;
    int k = 0;
    while ((token = tokenizer.nextToken()) != nullptr)
    {
        destAddresses.push_back(atoi(token));
        k++;
    }

    if (destAddresses.size() == 0)
        throw cRuntimeError("At least one address must be "
                            "in the destAddresses parameter!");

    timeOut = 0.002;
    alenSignal = registerSignal("bufferLen");
    busySignal = registerSignal("busy");

    emit(alenSignal, buffer.size());
    emit(busySignal, false);
    isBusy = false;

    endToEndDelaySignal = registerSignal("endToEndDelay");
    arrivalSignal = registerSignal("arrival");
}

void Bundle::handleMessage(cMessage *msg)
{
    if (msg == endTransmissionEvent)
    { // Transmission finished, we can start next one.
        EV << "Transmission finished.\n";
        isBusy = false;
        if (buffer.size() == 0)
        {
            emit(busySignal, false);
        }
        else
        { // resending message in Array
            for (int index = 0; index < buffer.size(); index++)
            {
                msg = (Packet *)buffer[index];

                Packet *copy = (Packet *)msg->dup();

                int numPk = copy->getNumberPk();
                globhopcountVector[numPk]++;
                startTransmitting(copy);
            }
        }
    }
    else
    { // arrived on gate "inBundle"
        Packet *pk = check_and_cast<Packet *>(msg);

        if (myAddress == destAddresses.size() - 1)
        { // final node
            int numPk = pk->getNumberPk();

            if (numPk == numPkCurrent)
            { // begin: numPkCurrent=0
                int index = buffer.add(pk);
            }
            else if (numPk == numPkNext)
            { // begin: numPkNext=1
                char pkname[40];
                sprintf(pkname, "pk-%d-to-%d-#%ld", pk->getSrcAddr(),
                        (int)destAddresses.size() - 1, (long)numPk - 1);
                simtime_t finalTime;

                for (int i = 0; i < buffer.size(); i++)
                { // delete message in bundle

                    int index = buffer.find(pkname);
                    Packet *pkOld = (Packet *)buffer[index];
                    simtime_t aux = pkOld->getArrivalTime();
                    finalTime = aux - (pkOld->getNumberPk() * timeOut);
                    delete buffer.remove(index);
                }

                EV << "globhopcountVector[" << numPk - 1 << "] = "
                   << globhopcountVector[numPk - 1]
                   << " hops" << endl;
                EV << "delay to arrived " << finalTime << endl;

                globhopcount = globhopcountVector[numPk - 1];
                // load results
                emit(arrivalSignal, globhopcount);
                emit(endToEndDelaySignal, finalTime);

                // clean globals variables
                globhopcount = 0;
                globhopcountVector[numPk - 1] = 0;

                int indexNew = buffer.add(pk);
                numPkCurrent++;
                numPkNext++;
            }
        }
        else if (pk->getDestAddr() == myAddress)
        { // arrived an ACK
            if (buffer.size() != 0)
            {
                cancelEvent(endTransmissionEvent);
                int numberPk = pk->getNumberPk();
                char pkname[40];
                sprintf(pkname, "pk-%d-to-%d-#%ld", myAddress,
                        (int)destAddresses.size() - 1,
                        (long)numberPk);

                int index = buffer.find(pkname);
                delete buffer.remove(index);
            }
        }
        else
        { // arrived a new message
            EV << "Bundle Received " << pk->getName() << endl;
            cancelEvent(endTransmissionEvent);
            // insert message in buffer
            int index = buffer.add(pk);
            emit(alenSignal, buffer.size());
            scheduleAt(simTime(), endTransmissionEvent);
        }
    }
}

void Bundle::startTransmitting(Packet *msg)
{
    EV << "Starting transmission of " << msg << endl;
    isBusy = true;
    cancelEvent(endTransmissionEvent);

    int64_t numBytes = msg->getByteLength();

    send(msg, "outBundle");

    simtime_t endTransmission = simTime() + timeOut;
    scheduleAt(endTransmission, endTransmissionEvent);
}

void Bundle::refreshDisplay() const
{
    getDisplayString().setTagArg("t", 0, isBusy ? "transmitting" : "idle");
    getDisplayString().setTagArg("i", 1,
                                 isBusy ? (buffer.size() >= 3 ? "red" : "yellow") : "");

    char buf[40];
    sprintf(buf, "bundle: %d", buffer.size());
    getDisplayString().setTagArg("t", 0, buf);
}
