# Trabajo especial de grado en Licenciatura en Ciencias de la Computación

Realizado por Ruben Danilo Capkob, bajo la dirección del Dr. Ing Pablo Alejandro Ferreyra, en la Universidad Nacional de Córdoba (UNC), facultad de Matemática, Astronomía, Física y Computación (FaMAF), Argentina.

## Titulo del trabajo

"Plataformas para redes tolerantes a demoras sobre nodos integrados en un chip"

## Requisitos

Tener instalado y funcionando OMNeT++.

## Instalación

Despues de clonar el repositorio:

```bash
mkdir <dir_name>
cd <dir_name>
git clone https://gitlab.com/danilocapkob/trabajo_especial_cs_danilocapkob.git
```

Crear desde OMNeT++ un proyecto para cada directorio (uno por modelo) y ejecutar el archivo **omnetpp.ini** de cada proyecto.

## OMNeT++

OMNeT ++ es un entorno de simulación de código público, basado en componentes, modular y de arquitectura abierta con un fuerte soporte de GUI y un kernel de simulación integrable. Su área de aplicación principal es la simulación de redes de comunicación, pero se ha utilizado con éxito en otras áreas como la simulación de sistemas de TI, redes de colas, arquitecturas de hardware y procesos comerciales.

Consulte el sitio web principal de OMNeT ++ <https://omnetpp.org/> para obtener documentación, tutoriales y otros materiales introductorios, descargas de versiones, catálogo de modelos y otra información útil.

## License

OMNeT++ se distribuye bajo el **Academic Public License**.
