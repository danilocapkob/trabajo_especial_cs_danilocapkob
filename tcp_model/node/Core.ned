//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 1992-2015 Andras Varga
// Copyright (C) 2020 Ruben Danilo Capkob. (danilocapkob@gmail.com)
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

package node;

simple Core like ICore
{
    parameters:
        int address;  // local node address
        string destAddresses;  // destination addresses

        // length of one message (fixed! no "volatile" modifier)
        volatile int packetLength @unit(byte);
        @display("i=block/circle");
        
        @signal[sourceAddress](type="long");
        
        @signal[endToEndDelay](type="simtime_t");
        @statistic[endToEndDelay](title="end-to-end delay of arrived packets";
                                  unit=s;
                                  record=vector, stats, mean, min, max;
                                  interpolationmode=none);
        
        @signal[arrival](type="long");
        @statistic[hopCount](title="hop count";
                             source="arrival";
                             record=vector, stats;
                             interpolationmode=none);

        @statistic[sourceAddress](title="source address of arrived packets";
                                  interpolationmode=none;record=vector?);
    gates:
        input in;
        output out;
}


