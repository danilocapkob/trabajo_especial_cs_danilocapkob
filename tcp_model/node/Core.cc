//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 1992-2015 Andras Varga
// Copyright (C) 2020 Ruben Danilo Capkob. (danilocapkob@gmail.com)
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#ifdef _MSC_VER
#pragma warning(disable : 4786)
#endif

#include <vector>
#include <omnetpp.h>
#include "Packet_m.h"

using namespace omnetpp;

int globhopcount = 0;
std::vector<int> globhopcountVector;

/**
 * Generates traffic for the network.
 */
class Core : public cSimpleModule
{
private:
    // configuration
    int myAddress;
    std::vector<int> destAddresses;
    cQueue buffer;
    cPar *packetLengthBytes;

    // state
    cMessage *generatePacket;
    cMessage *resendingPacket;
    long pkCounter;

    // wait time
    simtime_t timeOut;

    // signals
    simsignal_t endToEndDelaySignal;
    simsignal_t arrivalSignal;
    simsignal_t sourceAddressSignal;

public:
    Core();
    virtual ~Core();

protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
    virtual Packet *generateMessage(int dstAddress);
};

Define_Module(Core);

Core::Core()
{
    generatePacket = nullptr;
}

Core::~Core()
{
    cancelAndDelete(generatePacket);
}

void Core::initialize()
{
    buffer.setName("buffer");
    myAddress = par("address");
    packetLengthBytes = &par("packetLength");
    pkCounter = 0;
    const char *destAddressesPar = par("destAddresses");
    cStringTokenizer tokenizer(destAddressesPar);
    const char *token;

    int k = 0;
    while ((token = tokenizer.nextToken()) != nullptr)
    {
        destAddresses.push_back(atoi(token));
        k++;
    }

    if (destAddresses.size() == 0)
        throw cRuntimeError("At least one address must be "
                            "in the destAddresses parameter!");

    if (myAddress == 0)
    { // The only node that sends packets
        timeOut = 0.006;
        generatePacket = new cMessage("nextPacket");
        generatePacket->setSchedulingPriority(2);

        resendingPacket = new cMessage("resendingPacket");
        resendingPacket->setSchedulingPriority(2);

        scheduleAt(0, generatePacket);
    }

    endToEndDelaySignal = registerSignal("endToEndDelay");
    arrivalSignal = registerSignal("arrival");
    sourceAddressSignal = registerSignal("sourceAddress");
}

void Core::handleMessage(cMessage *msg)
{
    if (msg == generatePacket)
    {
        // Sending packet
        int destAddress = destAddresses.size() - 1;
        Packet *pk = generateMessage(destAddress);
        globhopcountVector.push_back(0);

        // We record the creation time of a new package
        pk->setTimestamp();
        buffer.insert(pk);

        Packet *copy = (Packet *)buffer.front()->dup();
        send(copy, "out");

        scheduleAt(simTime() + timeOut, resendingPacket);
        if (hasGUI())
        {
            getParentModule()->bubble("Generating packet...");
        }
    }
    else if (msg == resendingPacket)
    {
        msg = (Packet *)buffer.front()->dup();
        Packet *pk = check_and_cast<Packet *>(msg);
        send(pk, "out");
        scheduleAt(simTime() + timeOut, resendingPacket);
    }
    else
    { // Handle incoming packet
        Packet *pk = check_and_cast<Packet *>(msg);
        int numPk = pk->getNumPk();

        if (myAddress == 0 && pk->getDestAddr() == 0)
        {
            cancelEvent(resendingPacket);
            // Record the package arrival time

            emit(endToEndDelaySignal, simTime() - pk->getCreationTime());
            emit(sourceAddressSignal, pk->getSrcAddr());

            globhopcount = globhopcountVector[numPk];
            emit(arrivalSignal, globhopcount);

            EV << "globHopCount = " << globhopcount << endl;
            EV << "endToEndDelaySignal = " << simTime() - pk->getCreationTime() << endl;

            Packet *delPk = (Packet *)buffer.pop();
            delete delPk;

            globhopcountVector[numPk] = 0;
            globhopcount = 0;
            delete pk;
            scheduleAt(simTime(), generatePacket);
        }
        else if (myAddress == destAddresses.size() - 1)
        {
            pk->setDestAddr(0);
            pk->setSrcAddr(myAddress);
            int dstAddress = pk->getDestAddr();
            char pkname[40];
            sprintf(pkname, "ack-%d-to-%d-#%ld",
                    myAddress, dstAddress, pk->getNumPk());
            pk->setName(pkname);
            pk->setSchedulingPriority(0);
            send(pk, "out");
        }

        if (hasGUI())
            getParentModule()->bubble("Arrived!");
    }
}

Packet *Core::generateMessage(int dstAddress)
{
    char pkname[40];
    sprintf(pkname, "pk-%d-to-%d-#%ld", myAddress, dstAddress, pkCounter);
    EV << "generating packet " << pkname << endl;

    Packet *pk = new Packet(pkname);
    pk->setByteLength(packetLengthBytes->intValue());
    pk->setKind(intuniform(0, 7));
    pk->setSrcAddr(myAddress);
    pk->setDestAddr(dstAddress);
    pk->setNumPk(pkCounter);
    pk->setSchedulingPriority(1);
    pkCounter++;

    return pk;
}
